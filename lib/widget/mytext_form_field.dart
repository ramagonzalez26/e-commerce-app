import 'package:flutter/material.dart';

class MyTextFormField extends StatelessWidget {
  const MyTextFormField(
      {required this.name,
      required this.validator,
      required this.icon,
      required this.onChanged});

  final Function(String?) validator;
  final Function(String?) onChanged;
  final String name;
  final Widget icon;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (String? value) => validator(value),
      onChanged: onChanged,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: name,
        icon: icon,
      ),
    );
  }
}
