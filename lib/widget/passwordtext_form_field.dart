import 'package:flutter/material.dart';

class PasswordTextFormField extends StatelessWidget {
  const PasswordTextFormField(
      {required this.name,
      required this.obserText,
      required this.validator,
      required this.onTap,
      required this.onChanged});

  final bool obserText;
  final Function(String?) validator;
  final String name;
  final VoidCallback onTap;
  final Function(String?) onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obserText,
      validator: (String? value) => validator(value),
      onChanged: onChanged,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: name,
        icon: Icon(Icons.key),
        suffixIcon: GestureDetector(
          onTap: onTap,
          child: Icon(
            obserText == true ? Icons.visibility : Icons.visibility_off,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
