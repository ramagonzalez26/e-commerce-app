import 'package:flutter/material.dart';

class SingleProduct extends StatelessWidget {
  SingleProduct(
      {Key? key,
      required this.image,
      required this.price,
      required this.namaBarang});

  final String image;
  final double price;
  final String namaBarang;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        //color: Colors.yellow,
        height: 200,
        width: 150,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 3),
              child: Container(
                height: 150,
                width: 140,
                decoration: BoxDecoration(
                    //color: Colors.grey,
                    image: DecorationImage(
                        fit: BoxFit.fill, image: NetworkImage(image))),
              ),
            ),
            Text(
              "\$" + price.toString(),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: Color(0xff9b96d6)),
            ),
            Text(
              namaBarang,
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
