import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  VoidCallback onpressed;
  final String name;
  MyButton({required this.name, required this.onpressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      width: double.infinity,
      child: ElevatedButton(
        onPressed: onpressed,
        child: Text(name),
        style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
      ),
    );
  }
}
