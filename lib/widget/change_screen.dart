import 'package:flutter/material.dart';

class ChangeScreen extends StatelessWidget {
  ChangeScreen(
      {required this.name, required this.onTap, required this.whichAccount});

  final String whichAccount;
  final String name;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(whichAccount),
        SizedBox(
          width: 5,
        ),
        GestureDetector(
          onTap: onTap,
          child: Text(
            name,
            style: TextStyle(
                color: Colors.cyan, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
