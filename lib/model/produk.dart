import 'package:flutter/material.dart';

class Produk {
  final String name;
  final String image;
  final double price;

  Produk({required this.name, required this.image, required this.price});
}
