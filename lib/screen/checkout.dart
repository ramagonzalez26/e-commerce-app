import 'dart:ui';

import 'package:belajar_futter/Screen/homepage.dart';
import 'package:belajar_futter/screen/cartscreen.dart';
import 'package:flutter/material.dart';

class CheckOut extends StatefulWidget {
  const CheckOut(
      {Key? key,
      required this.image,
      required this.namaBarang,
      required this.price,
      required this.quantity})
      : super(key: key);
  final String image;
  final String namaBarang;
  final double price;
  final int quantity;

  @override
  State<CheckOut> createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {
  int count = 1;
  final TextStyle myStyle = TextStyle(fontSize: 18, color: Colors.black);
  Widget _buildSingleCartProduct() {
    return Container(
      height: 150,
      width: double.infinity,
      child: Card(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    height: 100,
                    width: 150,
                    // color: Colors.amber,
                    decoration: BoxDecoration(
                      // color: Colors.amber,
                      image: DecorationImage(
                          fit: BoxFit.fill, image: NetworkImage(widget.image)),
                    ),
                  ),
                  Container(
                    height: 140,
                    width: 170,
                    child: ListTile(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(widget.namaBarang),
                          Text("Jam"),
                          Text("\$ ${widget.price.toString()}",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xff9b96d6),
                                  fontWeight: FontWeight.bold)),
                          Container(
                            height: 35,
                            width: 100,
                            // color: Color(0xfff2f2f2),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Quantity"),
                                Text(widget.quantity.toString())
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ]),
      ),
    );
  }

  Widget _buildBottomDetail(String startName, String endName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          startName,
          style: myStyle,
        ),
        Text(endName, style: myStyle),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          height: 45,
          width: 100,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: () {
              // Navigator.of(context).pushReplacement(MaterialPageRoute(
              //     builder: (context) => CartScreen(
              //           image: widget.image,
              //           namaBarang: widget.namaBarang,
              //           price: widget.price,
              //         )));
            },
            child: Text(
              "Beli ",
              style: myStyle,
            ),
            style: ElevatedButton.styleFrom(
                primary: Color.fromARGB(255, 224, 119, 154),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
          )),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "CheckOut Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => CartScreen(
                      image: widget.image,
                      namaBarang: widget.namaBarang,
                      price: widget.price / widget.quantity,
                    )));
          },
        ),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none, color: Colors.black)),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: ListView(
          children: <Widget>[
            // Container(
            //   height: 50,
            //   width: double.infinity,
            //   child: Column(
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     mainAxisAlignment: MainAxisAlignment.end,
            //     children: <Widget>[
            //       Text(
            //         "CheckOut",
            //         style: TextStyle(fontSize: 20),
            //       ),
            //     ],
            //   ),
            // ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                _buildSingleCartProduct(),
                Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      _buildBottomDetail(
                          "Harga", "\$ ${widget.price.toString()}}"),
                      _buildBottomDetail("Discount", "3%"),
                      _buildBottomDetail(
                          "Shipping", "\$ ${(widget.price * 2).toString()}"),
                      _buildBottomDetail("Total",
                          "\$ ${(widget.price * (2) + (widget.price * (97 / 100))).toString()}"),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
