import 'package:belajar_futter/Screen/homepage.dart';
import 'package:belajar_futter/screen/checkout.dart';
import 'package:belajar_futter/screen/detailscreen.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatefulWidget {
  const CartScreen(
      {Key? key,
      required this.image,
      required this.namaBarang,
      required this.price})
      : super(key: key);

  final String image;
  final String namaBarang;
  final double price;

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  int count = 1;
  final TextStyle myStyle = TextStyle(fontSize: 18, color: Colors.black);

  Widget _buildSingleCartProduct() {
    return Container(
      height: 150,
      width: double.infinity,
      child: Card(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    height: 100,
                    width: 150,
                    // color: Colors.amber,
                    decoration: BoxDecoration(
                      // color: Colors.amber,
                      image: DecorationImage(
                          fit: BoxFit.fill, image: NetworkImage(widget.image)),
                    ),
                  ),
                  Container(
                    height: 140,
                    width: 200,
                    child: ListTile(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(widget.namaBarang),
                          Text("Jam"),
                          Text(
                              "\$ ${(widget.price * count.toDouble()).toString()}",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(0xff9b96d6),
                                  fontWeight: FontWeight.bold)),
                          Container(
                            height: 35,
                            width: 120,
                            color: Color(0xfff2f2f2),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        count--;
                                        if (count < 0) {
                                          count = 0;
                                        }
                                      });
                                    },
                                    child: Icon(
                                      Icons.remove,
                                      color: Colors.black,
                                    )),
                                Text(
                                  count.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        count++;
                                      });
                                    },
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.black,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          height: 45,
          width: 100,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: ElevatedButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => CheckOut(
                        image: widget.image,
                        namaBarang: widget.namaBarang,
                        price: widget.price * count.toDouble(),
                        quantity: count,
                      )));
            },
            child: Text(
              "Lanjutkan ",
              style: myStyle,
            ),
            style: ElevatedButton.styleFrom(
                primary: Color.fromARGB(255, 224, 119, 154),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
          )),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Cart Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => DetailScreen(
                      image: widget.image,
                      namaBarang: widget.namaBarang,
                      price: widget.price,
                    )));
          },
        ),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                color: Colors.black,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none, color: Colors.black)),
        ],
      ),
      body: ListView(
        children: [
          //container chart
          _buildSingleCartProduct(),
          // _buildSingleCartProduct(),
          // _buildSingleCartProduct(),
          // _buildSingleCartProduct(),
          // _buildSingleCartProduct(),
          // _buildSingleCartProduct(),
        ],
      ),
    );
  }
}
