import 'package:belajar_futter/Screen/login.dart';
import 'package:belajar_futter/widget/mytext_form_field.dart';
import 'package:belajar_futter/widget/passwordtext_form_field.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../widget/change_screen.dart';
import '../widget/mybutton.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
String p =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
RegExp regExp = RegExp(p);
bool obserText = true;
String nama = '';
String noHp = '';
String email = '';
String password = '';
String? errorMessage;

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 220,
                  width: double.infinity,
                  //color: Colors.blueAccent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Daftar",
                        style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 400,
                  width: double.infinity,
                  //color: Colors.blue,
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      // MyTextFormField(
                      //     name: "Username",
                      //     validator: (value) {
                      //       if (value == "") {
                      //         return "Kolom nama tidak boleh kosong";
                      //       } else if (value!.length < 6) {
                      //         return "nama terlalu pendel";
                      //       }
                      //       return "";
                      //     },
                      //     onChanged: (value) {
                      //       setState(() {
                      //         nama = value!;
                      //         print(nama);
                      //       });
                      //     },
                      //     icon: Icon(Icons.person)),
                      MyTextFormField(
                          name: "Email",
                          validator: (value) {
                            if (value == "") {
                              return "Kolom email tidak boleh kosong";
                            } else if (!regExp.hasMatch(value!)) {
                              return "Email is invalid";
                            }
                            return "";
                          },
                          onChanged: (value) {
                            setState(() {
                              email = value!;
                            });
                          },
                          icon: Icon(Icons.email)),
                      PasswordTextFormField(
                          name: "Kata Sandi",
                          obserText: obserText,
                          validator: (value) {
                            if (value == "") {
                              return "Kolom kata sandi tidak boleh kosong";
                            } else if (value!.length < 8) {
                              return "Password terlalu pendek";
                            }
                            return "";
                          },
                          onChanged: (value) {
                            setState(() {
                              password = value!;
                            });
                          },
                          onTap: () {
                            setState(() {
                              obserText = !obserText;
                            });
                            FocusScope.of(context).unfocus();
                          }),
                      // MyTextFormField(
                      //     name: "Nomor HP",
                      //     validator: (value) {
                      //       if (value == "") {
                      //         return "Kolom Nomor HPtidak boleh kosong";
                      //       } else if (value!.length < 11) {
                      //         return "Nomor HP terlalu pendek";
                      //       }
                      //       return "";
                      //     },
                      //     onChanged: (value) {
                      //       setState(() {
                      //         noHp = value!;
                      //         print(noHp);
                      //       });
                      //     },
                      //     icon: Icon(Icons.phone)),
                      MyButton(
                          name: "Daftar",
                          onpressed: () {
                            validation();
                          }),
                      ChangeScreen(
                        name: "Login",
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => LoginScreen()));
                        },
                        whichAccount: "Sudah mempunyai akun",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void validation() async {
    final FormState? _form = _formKey.currentState;
    if (!_form!.validate()) {
      try {
        await FirebaseAuth.instance
            .createUserWithEmailAndPassword(email: email, password: password);
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "weak-password":
            errorMessage = "Password terlalu singkat.";
            break;
          case "email-already-in-use":
            errorMessage = "Akun dengan email ini sudah ada.";
            break;
          case "invalid-email":
            errorMessage = "Email yang dimasukan tidak valid.";
            break;
          case "wrong-password":
            errorMessage = "Password/Email yang dimasukan tidak valid.";
            break;
          case "user-not-found":
            errorMessage = "User with this email doesn't exist.";
            break;
          case "user-disabled":
            errorMessage = "Anda sudah di banned.";
            break;
          case "too-many-requests":
            errorMessage = "Too many requests";
            break;
          case "operation-not-allowed":
            errorMessage = "Signing in with Email and Password is not enabled.";
            break;

          default:
            errorMessage = "Semua Kolom input harus diisi.";
            break;
        }
        Fluttertoast.showToast(msg: errorMessage!);

        ///print(error.code);
      }
    } else {
      //print("No");
    }
  }
}
