import 'package:belajar_futter/screen/cartscreen.dart';
import 'package:belajar_futter/screen/homepage.dart';
import 'package:belajar_futter/screen/listproduct.dart';
import 'package:belajar_futter/screen/signup.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  DetailScreen(
      {Key? key,
      required this.image,
      required this.price,
      required this.namaBarang})
      : super(key: key);

  final String image;
  final String namaBarang;
  final double price;

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  int count = 1;
  Widget _buildSizeProduct(String name) {
    return Container(
      height: 60,
      width: 60,
      color: Color.fromARGB(23, 16, 192, 236),
      child: Center(
        child: Text(
          name,
          style: myStyle,
        ),
      ),
    );
  }

  Widget _buildColorProduct(Color color) {
    return Container(
      height: 60,
      width: 60,
      color: color,
    );
  }

  final TextStyle myStyle = TextStyle(fontSize: 18, color: Colors.black);

  Widget _buildImage() {
    return Center(
      child: Container(
        width: 300,
        child: Card(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Container(
              height: 220,
              decoration: BoxDecoration(
                //color: Colors.amber,
                image: DecorationImage(
                    fit: BoxFit.fill, image: NetworkImage(widget.image)),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNameToDescriptionPart() {
    return Container(
      height: 80,
      //color: Colors.amber,
      child: Row(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.namaBarang,
                style: myStyle,
              ),
              Text(
                "\$ ${widget.price.toString()}",
                style: TextStyle(fontSize: 18, color: Color(0xff9b96d6)),
              ),
              Text(
                "Deskripsi",
                style: myStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildDescription() {
    return Container(
      height: 150,
      //color: Colors.blue,
      child: Wrap(
        children: <Widget>[
          Text(
            "Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks . ",
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }

  Widget _buildWarnaPart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Text(
          "Warna",
          style: myStyle,
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          width: 270,
          //color: Color.fromARGB(23, 16, 192, 236),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _buildColorProduct(Color.fromARGB(255, 96, 183, 253)),
              SizedBox(
                width: 1,
              ),
              _buildColorProduct(Color.fromARGB(255, 136, 201, 155)),
              SizedBox(
                width: 1,
              ),
              _buildColorProduct(Color.fromARGB(255, 248, 248, 126)),
              SizedBox(
                width: 1,
              ),
              _buildColorProduct(Color.fromARGB(255, 19, 138, 235)),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSizePart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Size",
          style: myStyle,
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          width: 265,
          //color: Color.fromARGB(23, 16, 192, 236),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _buildSizeProduct("S"),
              SizedBox(
                width: 1,
              ),
              _buildSizeProduct("M"),
              SizedBox(
                width: 1,
              ),
              _buildSizeProduct("L"),
              SizedBox(
                width: 1,
              ),
              _buildSizeProduct("XL"),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildQuantityPart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        Text(
          "Quantity",
          style: myStyle,
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          height: 40,
          width: 130,
          //color: Colors.cyan,
          decoration: BoxDecoration(
              color: Colors.cyan, borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                  onTap: () {
                    setState(() {
                      count--;
                      if (count < 0) {
                        count = 0;
                      }
                    });
                  },
                  child: Icon(
                    Icons.remove,
                    color: Colors.black,
                  )),
              Text(
                count.toString(),
                style: myStyle,
              ),
              GestureDetector(
                  onTap: () {
                    setState(() {
                      count++;
                    });
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.black,
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildButtoPart() {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        Container(
            height: 50,
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => CartScreen(
                          image: widget.image,
                          namaBarang: widget.namaBarang,
                          price: widget.price,
                        )));
              },
              child: Text(
                "Beli ",
                style: myStyle,
              ),
              style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 224, 119, 154),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
            )),
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              "Total harga\t :",
              style: myStyle,
            ),
            Text(
              "\$ " + (widget.price * count.toDouble()).toString(),
              style: TextStyle(fontSize: 18, color: Color(0xff9b96d6)),
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Detail Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => Homepage()));
          },
        ),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                color: Colors.black,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none, color: Colors.black)),
        ],
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            _buildImage(),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //nama & deskripsi
                  _buildNameToDescriptionPart(),
                  //deskripsi
                  _buildDescription(),
                  //size
                  _buildSizePart(),
                  //warna
                  _buildWarnaPart(),
                  //kuantitas
                  _buildQuantityPart(),
                  //tombol beli dan hitung total harga
                  _buildButtoPart(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
