import 'package:belajar_futter/Screen/homepage.dart';
import 'package:belajar_futter/Screen/signup.dart';
import 'package:belajar_futter/widget/mybutton.dart';
import 'package:belajar_futter/widget/change_screen.dart';
import 'package:belajar_futter/widget/mytext_form_field.dart';
import 'package:belajar_futter/widget/passwordtext_form_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
String p =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
RegExp regExp = RegExp(p);
bool obserText = true;
String? errorMessage;
String email = '';
String password = '';

// Widget _buildAllTextFormField(BuildContext context) {
//   return Container(
//     height: 170,
//     child: Column(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: <Widget>[

//       ],
//     ),
//   );
// }

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "LoginScreen",
                    style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  MyTextFormField(
                    name: "Email",
                    icon: Icon(Icons.email),
                    validator: (value) {
                      if (value == "") {
                        return "Kolom email tidak boleh kosong";
                      } else if (!regExp.hasMatch(value!)) {
                        return "Email is invalid";
                      }
                      return "";
                    },
                    onChanged: (value) {
                      setState(() {
                        email = value!;
                      });
                    },
                  ),
                  PasswordTextFormField(
                      name: "Password",
                      obserText: obserText,
                      validator: (value) {
                        if (value == "") {
                          return "Kolom kata sandi tidak boleh kosong";
                        } else if (value!.length < 8) {
                          return "Password terlalu pendek";
                        }
                        return "";
                      },
                      onChanged: (value) {
                        setState(() {
                          password = value!;
                        });
                      },
                      onTap: () {
                        setState(() {
                          obserText = !obserText;
                        });
                        FocusScope.of(context).unfocus();
                      }),
                  MyButton(
                      name: "Login",
                      onpressed: () {
                        validation();
                        //signIn(email, password);
                      }),
                  ChangeScreen(
                    name: "Daftar",
                    onTap: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => const SignUp()));
                    },
                    whichAccount: "Belum mempunyai akun",
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }

  void validation() async {
    final FormState? _form = _formKey.currentState;
    if (!_form!.validate()) {
      try {
        await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password)
            .then((uid) => {
                  Fluttertoast.showToast(msg: "LoginScreen Berhasil"),
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => Homepage()),
                      (Route<dynamic> route) => false),
                });
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage = "Password/Email yang dimasukan tidak valid.";
            break;
          case "wrong-password":
            errorMessage = "Password/Email yang dimasukan tidak valid.";
            break;
          case "user-not-found":
            errorMessage = "User dengan email tersebut tidak ditemukan.";
            break;
          case "user-disabled":
            errorMessage = "User with this email has been disabled.";
            break;
          case "too-many-requests":
            errorMessage = "Too many requests";
            break;
          case "operation-not-allowed":
            errorMessage = "Signing in with Email and Password is not enabled.";
            break;
          default:
            errorMessage = "Semua Kolom input harus diisi.";
        }
        Fluttertoast.showToast(msg: errorMessage!);
        print(error.code);
      }
    } else {
      print("No");
    }
  }
}
