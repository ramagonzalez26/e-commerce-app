import 'package:belajar_futter/model/produk.dart';
import 'package:belajar_futter/screen/detailscreen.dart';
import 'package:belajar_futter/screen/homepage.dart';
import 'package:belajar_futter/widget/singleproduct.dart';
import 'package:flutter/material.dart';

class ListProduct extends StatelessWidget {
  ListProduct({Key? key, required this.nama, required this.mySnapshot})
      : super(key: key);

  final String nama;
  final List<Produk> mySnapshot;

  Widget _buildCategoryProduct(String image, int color) {
    return CircleAvatar(
      maxRadius: 30,
      backgroundColor: Color(color),
      child: Container(
        height: 50,
        child: Image(
          color: Colors.white,
          image: AssetImage("images/" + image),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Product Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back, color: Colors.black),
        //   onPressed: () {
        //     Navigator.of(context).pushReplacement(
        //         MaterialPageRoute(builder: (context) => Homepage()));
        //   },
        // ),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                color: Colors.black,
              )),
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_none, color: Colors.black)),
        ],
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  height: 50,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            nama,
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 550,
                  child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 0.65,
                    scrollDirection: Axis.vertical,
                    children: mySnapshot
                        .map(
                          (e) => GestureDetector(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => DetailScreen(
                                          image: e.image,
                                          price: e.price,
                                          namaBarang: e.name)));
                            },
                            child: SingleProduct(
                                image: e.image,
                                price: e.price,
                                namaBarang: e.name),
                          ),
                        )
                        .toList(),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
