import 'package:belajar_futter/model/produk.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ProductProvider with ChangeNotifier {
  late Produk data;
  List<Produk> feature = [];
  List<Produk> homeFeature = [];
  List<Produk> newArchive = [];

  Future<void> getFeatureData() async {
    List<Produk> newList = [];
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection("product")
        .doc("ZUXxE3YC5GzQlQhGb12X")
        .collection("featureproduk")
        .get();
    snapshot.docs.forEach(
      (element) {
        data = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(data);
      },
    );
    feature = newList;
    // notifyListeners();
  }

  Future<void> getNewArchiveData() async {
    List<Produk> newList = [];
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection("product")
        .doc("ZUXxE3YC5GzQlQhGb12X")
        .collection("newarchived")
        .get();
    snapshot.docs.forEach(
      (element) {
        data = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(data);
      },
    );
    newArchive = newList;
    // notifyListeners();
  }

  Future<void> getHomeFeatureData() async {
    List<Produk> newList = [];
    QuerySnapshot snapshot =
        await FirebaseFirestore.instance.collection("homefeature").get();
    snapshot.docs.forEach(
      (element) {
        data = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(data);
      },
    );
    homeFeature = newList;
    // notifyListeners();
  }

  List<Produk> get getHomeFeatureList {
    return homeFeature;
  }

  List<Produk> get getNewArchiveList {
    return newArchive;
  }

  List<Produk> get getFeatureList {
    return feature;
  }
}
