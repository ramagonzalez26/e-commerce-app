import 'package:belajar_futter/model/produk.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CategoryProvider with ChangeNotifier {
  Produk? shirtData;
  List<Produk> shirt = [];

  Produk? shoesData;
  List<Produk> shoes = [];

  Produk? bagData;
  List<Produk> bags = [];

  Produk? pantsData;
  List<Produk> pants = [];

  Produk? jamData;
  List<Produk> jam = [];

  Future<void> getShirtData() async {
    List<Produk> newList = [];
    QuerySnapshot shirtSnapshot = await FirebaseFirestore.instance
        .collection("category")
        .doc("xhJqpyHyfK0qhsTkpnS6")
        .collection("shirt")
        .get();
    shirtSnapshot.docs.forEach(
      (element) {
        shirtData = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(shirtData!);
      },
    );
    shirt = newList;
  }

  Future<void> getShoesData() async {
    List<Produk> newList = [];
    QuerySnapshot shirtSnapshot = await FirebaseFirestore.instance
        .collection("category")
        .doc("xhJqpyHyfK0qhsTkpnS6")
        .collection("shoes")
        .get();
    shirtSnapshot.docs.forEach(
      (element) {
        shoesData = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(shirtData!);
      },
    );
    shoes = newList;
  }

  Future<void> getBagsData() async {
    List<Produk> newList = [];
    QuerySnapshot shirtSnapshot = await FirebaseFirestore.instance
        .collection("category")
        .doc("xhJqpyHyfK0qhsTkpnS6")
        .collection("bag")
        .get();
    shirtSnapshot.docs.forEach(
      (element) {
        bagData = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(bagData!);
      },
    );
    bags = newList;
  }

  Future<void> getPantsData() async {
    List<Produk> newList = [];
    QuerySnapshot shirtSnapshot = await FirebaseFirestore.instance
        .collection("category")
        .doc("xhJqpyHyfK0qhsTkpnS6")
        .collection("pants")
        .get();
    shirtSnapshot.docs.forEach(
      (element) {
        pantsData = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(pantsData!);
      },
    );
    pants = newList;
  }

  List<Produk> get getPantsList {
    return pants;
  }

  Future<void> getJamData() async {
    List<Produk> newList = [];
    QuerySnapshot shirtSnapshot = await FirebaseFirestore.instance
        .collection("category")
        .doc("xhJqpyHyfK0qhsTkpnS6")
        .collection("watch")
        .get();
    shirtSnapshot.docs.forEach(
      (element) {
        jamData = Produk(
            image: element.get("image"),
            name: element.get("name"),
            price: element.get("price"));
        newList.add(jamData!);
      },
    );
    jam = newList;
  }

  List<Produk> get getJamList {
    return jam;
  }

  List<Produk> get getBagsList {
    return bags;
  }

  List<Produk> get getShoesList {
    return shirt;
  }

  List<Produk> get getShirtList {
    return shirt;
  }
}
