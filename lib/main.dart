import 'package:belajar_futter/provider/category_provider.dart';
import 'package:belajar_futter/provider/product_provider.dart';
import 'package:belajar_futter/screen/cartscreen.dart';
import 'package:belajar_futter/screen/checkout.dart';
import 'package:belajar_futter/screen/detailscreen.dart';
import 'package:belajar_futter/screen/homepage.dart';
import 'package:belajar_futter/screen/listproduct.dart';
import 'package:belajar_futter/screen/login.dart';
import 'package:belajar_futter/screen/signup.dart';
import 'package:belajar_futter/screen/welcomescreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiProvider(
        providers: [
          ListenableProvider<ProductProvider>(
            create: (ctx) => ProductProvider(),
          ),
          ListenableProvider<CategoryProvider>(
            create: (ctx) => CategoryProvider(),
          )
        ],
        child: StreamBuilder(
            stream: FirebaseAuth.instance.authStateChanges(),
            builder: (ctx, snapShot) {
              if (snapShot.hasData) {
                return Homepage();
              } else {
                return LoginScreen();
              }
            }),
      ),

      // home: CartScreen(),
      //home: ListProduct(),
      // home: Homepage(),
      // home: WelcomeScreen(),
      // home: CheckOut(),
      //home: DetailScreen(),
      //home: Login(),
    );
  }
}
